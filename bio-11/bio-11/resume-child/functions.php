<?php
add_action( 'wp_enqueue_scripts', 'resume_child_enqueue_styles', 100 );
function resume_child_enqueue_styles() {
    wp_enqueue_style( 'resume-parent', get_theme_file_uri('/style.css') );
}
?>