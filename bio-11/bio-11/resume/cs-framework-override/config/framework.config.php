<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => constant('RESUME_THEME_NAME').' '.esc_html__('Options', 'resume'),
  'menu_type'       => 'theme', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'cs-framework',
  'ajax_save'       => true,
  'show_reset_all'  => false,
  'framework_title' => __('Designthemes Framework <small>by Designthemes</small>', 'resume'),
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

$options[]      = array(
  'name'        => 'general',
  'title'       => esc_html__('General', 'resume'),
  'icon'        => 'fa fa-gears',

  'fields'      => array(

	array(
	  'type'    => 'subheading',
	  'content' => esc_html__( 'General Options', 'resume' ),
	),

	array(
	  'id'  	 => 'show-pagecomments',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Globally Show Page Comments', 'resume'),
	  'info'	 => esc_html__('YES! to show comments on all the pages. This will globally override your "Allow comments" option under your page "Discussion" settings.', 'resume')
	),

	array(
	  'id'  	 => 'showall-pagination',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Show all pages in Pagination', 'resume'),
	  'info'	 => esc_html__('YES! to show all the pages instead of dots near the current page.', 'resume')
	),

	array(
	  'id'  	 => 'enable-stylepicker',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Style Picker', 'resume'),
	  'info'	 => esc_html__('YES! to show the style picker.', 'resume')
	),

	array(
	  'id'      => 'google-map-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Google Map API Key', 'resume'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid google account api key here', 'resume').'</p>',
	),

	array(
	  'id'      => 'mailchimp-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Mailchimp API Key', 'resume'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid mailchimp account api key here', 'resume').'</p>',
	),

  ),
);

$options[]      = array(
  'name'        => 'allpage_options',
  'title'       => esc_html__('All Page Options', 'resume'),
  'icon'        => 'fa fa-files-o',
  'sections' => array(

	// -----------------------------------------
	// Post Options
	// -----------------------------------------
	array(
	  'name'      => 'post_options',
	  'title'     => esc_html__('Post Options', 'resume'),
	  'icon'      => 'fa fa-file',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Single Post Options", 'resume' ),
		  ),
		
		  array(
			'id'  		 => 'single-post-authorbox',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Author Box', 'resume'),
			'info'		 => esc_html__('YES! to display author box in single blog posts.', 'resume')
		  ),

		  array(
			'id'  		 => 'single-post-related',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Related Posts', 'resume'),
			'info'		 => esc_html__('YES! to display related blog posts in single posts.', 'resume')
		  ),

		  array(
			'id'  		 => 'single-post-comments',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Posts Comments', 'resume'),
			'info'		 => esc_html__('YES! to display single blog post comments.', 'resume'),
			'default' 	 => true,
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Post Archives Page Layout", 'resume' ),
		  ),

		  array(
			'id'      	 => 'post-archives-page-layout',
			'type'       => 'image_select',
			'title'      => esc_html__('Page Layout', 'resume'),
			'options'    => array(
			  'content-full-width'   => RESUME_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => RESUME_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => RESUME_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => RESUME_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'post-archives-page-layout',
			),
		  ),

		  array(
			'id'  		 => 'show-standard-left-sidebar-for-post-archives',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Standard Left Sidebar', 'resume'),
			'dependency' => array( 'post-archives-page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  		 => 'show-standard-right-sidebar-for-post-archives',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Standard Right Sidebar', 'resume'),
			'dependency' => array( 'post-archives-page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Post Archives Post Layout", 'resume' ),
		  ),

		  array(
			'id'      	   => 'post-archives-post-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Post Layout', 'resume'),
			'options'      => array(
			  'one-column' 		  => RESUME_THEME_URI . '/cs-framework-override/images/one-column.png',
			  'one-half-column'   => RESUME_THEME_URI . '/cs-framework-override/images/one-half-column.png',
			  'one-third-column'  => RESUME_THEME_URI . '/cs-framework-override/images/one-third-column.png',
			),
			'default'      => 'one-half-column',
		  ),

		  array(
			'id'  		 => 'post-archives-enable-excerpt',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Allow Excerpt', 'resume'),
			'info'		 => esc_html__('YES! to allow excerpt', 'resume'),
			'default'    => true,
		  ),

		  array(
			'id'  		 => 'post-archives-excerpt',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Excerpt Length', 'resume'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Put Excerpt Length', 'resume').'</span>',
			'default' 	 => 40,
		  ),

		  array(
			'id'  		 => 'post-archives-enable-readmore',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Read More', 'resume'),
			'info'		 => esc_html__('YES! to enable read more button', 'resume'),
			'default'	 => true,
		  ),

		  array(
			'id'  		 => 'post-archives-readmore',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Read More Shortcode', 'resume'),
			'info'		 => esc_html__('Paste any button shortcode here', 'resume'),
			'default'	 => '[dt_sc_button title="Read More" style="filled" icon_type="fontawesome" iconalign="icon-right with-icon" iconclass="fa fa-long-arrow-right" class="type1"]',
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Single Post & Post Archive options", 'resume' ),
		  ),

		  array(
			'id'           => 'post-style',
			'type'         => 'select',
			'title'        => esc_html__('Post Style', 'resume'),
			'options'      => array(
			  'blog-default-style' 		=> esc_html__('Default', 'resume'),
			  'entry-date-left'      	=> esc_html__('Date Left', 'resume'),
			  'entry-date-author-left'  => esc_html__('Date and Author Left', 'resume'),
			  'blog-medium-style'       => esc_html__('Medium', 'resume'),
			  'blog-medium-style dt-blog-medium-highlight'     					 => esc_html__('Medium Hightlight', 'resume'),
			  'blog-medium-style dt-blog-medium-highlight dt-sc-skin-highlight'  => esc_html__('Medium Skin Highlight', 'resume'),
			),
			'class'        => 'chosen',
			'default'      => 'blog-default-style',
			'info'         => esc_html__('Choose post style to display single blog posts and archives.', 'resume'),
		  ),
		  
		  array(
			'id'      => 'post-format-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Post Format Meta', 'resume' ),
			'info'	  => esc_html__('YES! to show post format meta information', 'resume'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-author-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Author Meta', 'resume' ),
			'info'	  => esc_html__('YES! to show post author meta information', 'resume'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-date-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Date Meta', 'resume' ),
			'info'	  => esc_html__('YES! to show post date meta information', 'resume'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-comment-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Comment Meta', 'resume' ),
			'info'	  => esc_html__('YES! to show post comment meta information', 'resume'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-category-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Category Meta', 'resume' ),
			'info'	  => esc_html__('YES! to show post category information', 'resume'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-tag-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Tag Meta', 'resume' ),
			'info'	  => esc_html__('YES! to show post tag information', 'resume'),
			'default' => true
		  ),

		),
	),

	// -----------------------------------------
	// 404 Options
	// -----------------------------------------
	array(
	  'name'      => '404_options',
	  'title'     => esc_html__('404 Options', 'resume'),
	  'icon'      => 'fa fa-warning',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "404 Message", 'resume' ),
		  ),
		  
		  array(
			'id'      => 'enable-404message',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Message', 'resume' ),
			'info'	  => esc_html__('YES! to enable not-found page message.', 'resume'),
			'default' => true
		  ),

		  array(
			'id'           => 'notfound-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'resume'),
			'options'      => array(
			  'type1' 	   => esc_html__('Modern', 'resume'),
			  'type2'      => esc_html__('Classic', 'resume'),
			  'type4'  	   => esc_html__('Diamond', 'resume'),
			  'type5'      => esc_html__('Shadow', 'resume'),
			  'type6'      => esc_html__('Diamond Alt', 'resume'),
			  'type7'  	   => esc_html__('Stack', 'resume'),
			  'type8'  	   => esc_html__('Minimal', 'resume'),
			),
			'class'        => 'chosen',
			'default'      => 'type1',
			'info'         => esc_html__('Choose the style of not-found template page.', 'resume')
		  ),

		  array(
			'id'      => 'notfound-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('404 Dark BG', 'resume' ),
			'info'	  => esc_html__('YES! to use dark bg notfound page for this site.', 'resume')
		  ),

		  array(
			'id'           => 'notfound-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'resume'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'resume'),
			'info'       	 => esc_html__('Choose the page for not-found content.', 'resume')
		  ),
		  
		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Background Options", 'resume' ),
		  ),

		  array(
			'id'    => 'notfound_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'resume')
		  ),

		  array(
			'id'  		 => 'notfound-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'resume'),
			'info'		 => esc_html__('Paste custom CSS styles for not found page.', 'resume')
		  ),

		),
	),

	// -----------------------------------------
	// Underconstruction Options
	// -----------------------------------------
	array(
	  'name'      => 'comingsoon_options',
	  'title'     => esc_html__('Under Construction Options', 'resume'),
	  'icon'      => 'fa fa-thumbs-down',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Under Construction", 'resume' ),
		  ),
	
		  array(
			'id'      => 'enable-comingsoon',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Coming Soon', 'resume' ),
			'info'	  => esc_html__('YES! to check under construction page of your website.', 'resume')
		  ),
	
		  array(
			'id'           => 'comingsoon-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'resume'),
			'options'      => array(
			  'type1' 	   => esc_html__('Diamond', 'resume'),
			  'type2'      => esc_html__('Teaser', 'resume'),
			  'type3'  	   => esc_html__('Minimal', 'resume'),
			  'type4'      => esc_html__('Counter Only', 'resume'),
			  'type5'      => esc_html__('Belt', 'resume'),
			  'type6'  	   => esc_html__('Classic', 'resume'),
			  'type7'  	   => esc_html__('Boxed', 'resume')
			),
			'class'        => 'chosen',
			'default'      => 'type1',
			'info'         => esc_html__('Choose the style of coming soon template.', 'resume'),
		  ),

		  array(
			'id'      => 'uc-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('Coming Soon Dark BG', 'resume' ),
			'info'	  => esc_html__('YES! to use dark bg coming soon page for this site.', 'resume')
		  ),

		  array(
			'id'           => 'comingsoon-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'resume'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'resume'),
			'info'       	 => esc_html__('Choose the page for comingsoon content.', 'resume')
		  ),

		  array(
			'id'      => 'show-launchdate',
			'type'    => 'switcher',
			'title'   => esc_html__('Show Launch Date', 'resume' ),
			'info'	  => esc_html__('YES! to show launch date text.', 'resume'),
		  ),

		  array(
			'id'      => 'comingsoon-launchdate',
			'type'    => 'text',
			'title'   => esc_html__('Launch Date', 'resume'),
			'attributes' => array( 
			  'placeholder' => '10/30/2016 12:00:00'
			),
			'after' 	=> '<p class="cs-text-info">'.esc_html__('Put Format: 12/30/2016 12:00:00 month/day/year hour:minute:second', 'resume').'</p>',
		  ),

		  array(
			'id'           => 'comingsoon-timezone',
			'type'         => 'select',
			'title'        => esc_html__('UTC Timezone', 'resume'),
			'options'      => array(
			  '-12' => '-12', '-11' => '-11', '-10' => '-10', '-9' => '-9', '-8' => '-8', '-7' => '-7', '-6' => '-6', '-5' => '-5', 
			  '-4' => '-4', '-3' => '-3', '-2' => '-2', '-1' => '-1', '0' => '0', '+1' => '+1', '+2' => '+2', '+3' => '+3', '+4' => '+4',
			  '+5' => '+5', '+6' => '+6', '+7' => '+7', '+8' => '+8', '+9' => '+9', '+10' => '+10', '+11' => '+11', '+12' => '+12'
			),
			'class'        => 'chosen',
			'default'      => '0',
			'info'         => esc_html__('Choose utc timezone, by default UTC:00:00', 'resume'),
		  ),

		  array(
			'id'    => 'comingsoon_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'resume')
		  ),

		  array(
			'id'  		 => 'comingsoon-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'resume'),
			'info'		 => esc_html__('Paste custom CSS styles for under construction page.', 'resume'),
		  ),

		),
	),

  ),
);

// -----------------------------------------
// Widget area Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'widgetarea_options',
  'title'       => esc_html__('Widget Area', 'resume'),
  'icon'        => 'fa fa-trello',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Custom Widget Area for Sidebar", 'resume' ),
	  ),

	  array(
		'id'           => 'wtitle-style',
		'type'         => 'select',
		'title'        => esc_html__('Sidebar widget Title Style', 'resume'),
		'options'      => array(
		  'type1' 	   => esc_html__('Double Border', 'resume'),
		  'type2'      => esc_html__('Tooltip', 'resume'),
		  'type3'  	   => esc_html__('Title Top Border', 'resume'),
		  'type4'      => esc_html__('Left Border & Pattren', 'resume'),
		  'type5'      => esc_html__('Bottom Border', 'resume'),
		  'type6'  	   => esc_html__('Tooltip Border', 'resume'),
		  'type7'  	   => esc_html__('Boxed Modern', 'resume'),
		  'type8'  	   => esc_html__('Elegant Border', 'resume'),
		  'type9' 	   => esc_html__('Needle', 'resume'),
		  'type10' 	   => esc_html__('Ribbon', 'resume'),
		  'type11' 	   => esc_html__('Content Background', 'resume'),
		  'type12' 	   => esc_html__('Classic BG', 'resume'),
		  'type13' 	   => esc_html__('Tiny Boders', 'resume'),
		  'type14' 	   => esc_html__('BG & Border', 'resume'),
		  'type15' 	   => esc_html__('Classic BG Alt', 'resume'),
		  'type16' 	   => esc_html__('Left Border & BG', 'resume'),
		  'type17' 	   => esc_html__('Basic', 'resume'),
		  'type18' 	   => esc_html__('BG & Pattern', 'resume'),
		),
		'class'          => 'chosen',
		'default_option' => esc_html__('Choose any type', 'resume'),
		'info'           => esc_html__('Choose the style of sidebar widget title.', 'resume')
	  ),

	  array(
		'id'              => 'widgetarea-custom',
		'type'            => 'group',
		'title'           => esc_html__('Custom Widget Area', 'resume'),
		'button_title'    => esc_html__('Add New', 'resume'),
		'accordion_title' => esc_html__('Add New Widget Area', 'resume'),
		'fields'          => array(

		  array(
			'id'          => 'widgetarea-custom-name',
			'type'        => 'text',
			'title'       => esc_html__('Name', 'resume'),
		  ),

		)
	  ),

	),
);

// -----------------------------------------
// Woocommerce Options
// -----------------------------------------
if( function_exists( 'is_woocommerce' ) ){

	$options[]      = array(
	  'name'        => 'woocommerce_options',
	  'title'       => esc_html__('Woocommerce', 'resume'),
	  'icon'        => 'fa fa-shopping-cart',

	  'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Woocommerce Shop Page Options", 'resume' ),
		  ),

		  array(
			'id'  		 => 'shop-product-per-page',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Products Per Page', 'resume'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Number of products to show in catalog / shop page', 'resume').'</span>',
			'default' 	 => 12,
		  ),

		  array(
			'id'           => 'product-style',
			'type'         => 'select',
			'title'        => esc_html__('Product Style', 'resume'),
			'options'      => array(
			  'type1' 	   => esc_html__('Thick Border', 'resume'),
			  'type2'      => esc_html__('Pattern Overlay', 'resume'),
			  'type3'  	   => esc_html__('Thin Border', 'resume'),
			  'type4'      => esc_html__('Diamond Icons', 'resume'),
			  'type5'      => esc_html__('Girly', 'resume'),
			  'type6'  	   => esc_html__('Push Animation', 'resume'),
			  'type7' 	   => esc_html__('Dual Color BG', 'resume'),
			  'type8' 	   => esc_html__('Modern', 'resume'),
			  'type9' 	   => esc_html__('Diamond & Border', 'resume'),
			  'type10' 	   => esc_html__('Easing', 'resume'),
			  'type11' 	   => esc_html__('Boxed', 'resume'),
			  'type12' 	   => esc_html__('Easing Alt', 'resume'),
			  'type13' 	   => esc_html__('Parallel', 'resume'),
			  'type14' 	   => esc_html__('Pointer', 'resume'),
			  'type15' 	   => esc_html__('Diamond Flip', 'resume'),
			  'type16' 	   => esc_html__('Stack', 'resume'),
			  'type17' 	   => esc_html__('Bouncy', 'resume'),
			  'type18' 	   => esc_html__('Hexagon', 'resume'),
			  'type19' 	   => esc_html__('Masked Diamond', 'resume'),
			  'type20' 	   => esc_html__('Masked Circle', 'resume'),
			  'type21' 	   => esc_html__('Classic', 'resume'),
			),
			'class'        => 'chosen',
			'default' 	   => 'type1',
			'info'         => esc_html__('Choose products style to display shop & archive pages.', 'resume')
		  ),

		  array(
			'id'      	 => 'shop-page-product-layout',
			'type'       => 'image_select',
			'title'      => esc_html__('Product Layout', 'resume'),
			'options'    => array(
			  'one-half-column'     => RESUME_THEME_URI . '/cs-framework-override/images/one-half-column.png',
			  'one-third-column'    => RESUME_THEME_URI . '/cs-framework-override/images/one-third-column.png',
			  'one-fourth-column'   => RESUME_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
			),
			'default'      => 'one-third-column',
			'attributes'   => array(
			  'data-depend-id' => 'shop-page-product-layout',
			),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Detail Page Options", 'resume' ),
		  ),

		  array(
			'id'      	   => 'product-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'resume'),
			'options'      => array(
			  'content-full-width'   => RESUME_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => RESUME_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => RESUME_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => RESUME_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'resume'),
			'dependency'   	 => array( 'product-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'resume'),
			'dependency' 	 => array( 'product-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  		 	 => 'enable-related',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Related Products', 'resume'),
			'info'	  		 => esc_html__("YES! to display related products on single product's page.", 'resume')
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Category Page Options", 'resume' ),
		  ),

		  array(
			'id'      	   => 'product-category-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'resume'),
			'options'      => array(
			  'content-full-width'   => RESUME_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => RESUME_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => RESUME_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => RESUME_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-category-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-category-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'resume'),
			'dependency'   	 => array( 'product-category-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-category-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'resume'),
			'dependency' 	 => array( 'product-category-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),
		  
		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Tag Page Options", 'resume' ),
		  ),

		  array(
			'id'      	   => 'product-tag-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'resume'),
			'options'      => array(
			  'content-full-width'   => RESUME_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => RESUME_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => RESUME_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => RESUME_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-tag-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-tag-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'resume'),
			'dependency'   	 => array( 'product-tag-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-tag-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'resume'),
			'dependency' 	 => array( 'product-tag-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

	  ),
	);
}

// -----------------------------------------
// Hook Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'hook_options',
  'title'       => esc_html__('Hooks', 'resume'),
  'icon'        => 'fa fa-paperclip',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Top Hook", 'resume' ),
	  ),

	  array(
		'id'  	=> 'enable-top-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Top Hook', 'resume'),
		'info'	=> esc_html__("YES! to enable top hook.", 'resume')
	  ),

	  array(
		'id'  		 => 'top-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Top Hook', 'resume'),
		'info'		 => esc_html__('Paste your top hook, Executes after the opening &lt;body&gt; tag.', 'resume')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content Before Hook", 'resume' ),
	  ),

	  array(
		'id'  	=> 'enable-content-before-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content Before Hook', 'resume'),
		'info'	=> esc_html__("YES! to enable content before hook.", 'resume')
	  ),

	  array(
		'id'  		 => 'content-before-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content Before Hook', 'resume'),
		'info'		 => esc_html__('Paste your content before hook, Executes before the opening &lt;#primary&gt; tag.', 'resume')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content After Hook", 'resume' ),
	  ),

	  array(
		'id'  	=> 'enable-content-after-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content After Hook', 'resume'),
		'info'	=> esc_html__("YES! to enable content after hook.", 'resume')
	  ),

	  array(
		'id'  		 => 'content-after-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content After Hook', 'resume'),
		'info'		 => esc_html__('Paste your content after hook, Executes after the closing &lt;/#main&gt; tag.', 'resume')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Bottom Hook", 'resume' ),
	  ),

	  array(
		'id'  	=> 'enable-bottom-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Bottom Hook', 'resume'),
		'info'	=> esc_html__("YES! to enable bottom hook.", 'resume')
	  ),

	  array(
		'id'  		 => 'bottom-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Bottom Hook', 'resume'),
		'info'		 => esc_html__('Paste your bottom hook, Executes after the closing &lt;/body&gt; tag.', 'resume')
	  ),

   ),
);

// ------------------------------
// backup                       
// ------------------------------
$options[]   = array(
  'name'     => 'backup_section',
  'title'    => esc_html__('Backup', 'resume'),
  'icon'     => 'fa fa-shield',
  'fields'   => array(

    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'resume')
    ),

    array(
      'type'    => 'backup',
    ),

  )
);

// ------------------------------
// license
// ------------------------------
$options[]   = array(
  'name'     => 'theme_version',
  'title'    => constant('RESUME_THEME_NAME').esc_html__(' Log', 'resume'),
  'icon'     => 'fa fa-info-circle',
  'fields'   => array(

    array(
      'type'    => 'heading',
      'content' => constant('RESUME_THEME_NAME').esc_html__(' Theme Change Log', 'resume')
    ),
    array(
      'type'    => 'content',
      'content' => '<pre>
2017.09.04 - version 1.1
 * Optimised dummy content updated

2017.08.29 - version 1.0
 * First release!  </pre>',
    ),

  )
);

// ------------------------------
// Seperator
// ------------------------------
$options[] = array(
  'name'   => 'seperator_1',
  'title'  => esc_html__('Plugin Options', 'resume'),
  'icon'   => 'fa fa-plug'
);


CSFramework::instance( $settings, $options );