<?php
function resume_kirki_config() {
	return 'resume_kirki_config';
}

function resume_defaults( $key = '' ) {
	$defaults = array();

	# site identify
	$defaults['use-custom-logo'] = '1';
	$defaults['custom-logo'] = RESUME_THEME_URI.'/images/logo.png';
	$defaults['custom-dark-logo'] = RESUME_THEME_URI.'/images/light-logo.png';
	$defaults['site_icon'] = RESUME_THEME_URI.'/images/favicon.ico';
	$defaults['use-site-loader'] = '0';

	# site layout
	$defaults['site-layout'] = 'wide';

	# site skin
	$defaults['use-predefined-skin'] = '0';
	$defaults['predefined-skin'] = 'green';
	$defaults['primary-color'] = '#9ccc0b';
	$defaults['secondary-color'] = '#93bd12';
	$defaults['tertiary-color'] = '#31dae2';

	# site breadcrumb
	$defaults['show-breadcrumb'] = '1';
	$defaults['breadcrumb-delimiter'] = 'fa default';
	$defaults['breadcrumb-style'] = 'default';
	$defaults['customize-breadcrumb-title-typo'] = '1';
	$defaults['breadcrumb-title-typo'] = array( 'font-family' => 'Raleway',
		'variant' => 'regular',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '20px',
		'line-height' => '',
		'letter-spacing' => '0.5px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none' );
	$defaults['customize-breadcrumb-typo'] = '0';
	$defaults['breadcrumb-typo'] = array( 'font-family' => 'Raleway',
		'variant' => 'regular',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '13px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#333333',
		'text-align' => 'unset',
		'text-transform' => 'none' );

	# site header
	$defaults['header-type'] = 'fullwidth-header';
	$defaults['enable-sticy-nav'] = '1';
	$defaults['header-position'] = 'above slider';
	$defaults['header-transparency'] = 'default';
	$defaults['enable-header-darkbg'] = '0';
	$defaults['menu-search-icon'] = '1';
	$defaults['search-box-type'] = 'type2';
	$defaults['menu-cart-icon'] = '0';
	$defaults['enable-top-bar-content'] = '0';

	# site menu
	$defaults['menu-active-style'] =  'menu-active-highlight';
	$defaults['menu-hover-style'] =  '';

	# site footer
	$defaults['show-footer'] = '0';
	$defaults['footer-columns'] = '4';
	$defaults['customize-footer-title-typo'] = '0';
	$defaults['footer-title-typo'] = array( 'font-family' => 'Raleway',
		'variant' => '700',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '20px',
		'line-height' => '36px',
		'letter-spacing' => '0',
		'color' => '#2B2B2B',
		'text-align' => 'left',
		'text-transform' => 'none' );
	$defaults['customize-footer-content-typo'] = '0';
	$defaults['footer-content-typo'] = array( 'font-family' => 'Roboto',
		'variant' => 'regular',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '14px',
		'line-height' => '24px',
		'letter-spacing' => '0',
		'color' => '#333333',
		'text-align' => 'left',
		'text-transform' => 'none' );

	# site copyright
	$defaults['show-copyright-text'] = '1';
	$defaults['copyright-text'] = '&copy; 2017 Resume. All rights reserved. Design by <a title="" href="http://themeforest.net/user/designthemes">DesignThemes</a>';
	$defaults['enable-copyright-darkbg'] = '1';
	$defaults['copyright-next'] = 'disable';
	$defaults['customize-footer-copyright-bg'] = '0';
	$defaults['customize-footer-copyright-text-typo'] = '0';
	$defaults['customize-footer-menu-typo'] = '0';

	# site social
	$defaults['social-facebook'] = '#';
	$defaults['social-twitter'] = '#';
	$defaults['social-google-plus'] = '#';
	$defaults['social-instagram'] = '#';

	# site typography
	$defaults['customize-body-h1-typo'] = '1';
	$defaults['h1'] = array(
		'font-family' => 'Raleway',
		'variant' => '700',
		'font-size' => '30px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h2-typo'] = '1';
	$defaults['h2'] = array(
		'font-family' => 'Raleway',
		'variant' => '700',
		'font-size' => '24px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h3-typo'] = '1';
	$defaults['h3'] = array(
		'font-family' => 'Raleway',
		'variant' => '700',
		'font-size' => '18px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h4-typo'] = '1';
	$defaults['h4'] = array(
		'font-family' => 'Raleway',
		'variant' => '700',
		'font-size' => '16px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h5-typo'] = '1';
	$defaults['h5'] = array(
		'font-family' => 'Raleway',
		'variant' => '100',
		'font-size' => '14px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h6-typo'] = '1';
	$defaults['h6'] = array(
		'font-family' => 'Raleway',
		'variant' => '700',
		'font-size' => '13px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-menu-typo'] = '1';
	$defaults['menu-typo'] = array(
		'font-family' => 'Raleway',
		'variant' => '600',
		'font-size' => '16px',
		'line-height' => '',
		'letter-spacing' => '0.5px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-content-typo'] = '1';
	$defaults['body-content-typo'] = array(
		'font-family' => 'Roboto',
		'variant' => '300',
		'font-size' => '18px',
		'line-height' => '30px',
		'letter-spacing' => '',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);

	if( !empty( $key ) && array_key_exists( $key, $defaults) ) {
		return $defaults[$key];
	}

	return '';
}

function resume_image_positions() {

	$positions = array( "top left" => esc_attr__('Top Left','resume'),
		"top center"    => esc_attr__('Top Center','resume'),
		"top right"     => esc_attr__('Top Right','resume'),
		"center left"   => esc_attr__('Center Left','resume'),
		"center center" => esc_attr__('Center Center','resume'),
		"center right"  => esc_attr__('Center Right','resume'),
		"bottom left"   => esc_attr__('Bottom Left','resume'),
		"bottom center" => esc_attr__('Bottom Center','resume'),
		"bottom right"  => esc_attr__('Bottom Right','resume'),
	);

	return $positions;
}

function resume_image_repeats() {

	$image_repeats = array( "repeat" => esc_attr__('Repeat','resume'),
		"repeat-x"  => esc_attr__('Repeat in X-axis','resume'),
		"repeat-y"  => esc_attr__('Repeat in Y-axis','resume'),
		"no-repeat" => esc_attr__('No Repeat','resume')
	);

	return $image_repeats;
}

function resume_border_styles() {

	$image_repeats = array(
		"none"	 => esc_attr__('None','resume'),
		"dotted" => esc_attr__('Dotted','resume'),
		"dashed" => esc_attr__('Dashed','resume'),
		"solid"	 => esc_attr__('Solid','resume'),
		"double" => esc_attr__('Double','resume'),
		"groove" => esc_attr__('Groove','resume'),
		"ridge"	 => esc_attr__('Ridge','resume'),
	);

	return $image_repeats;
}

function resume_animations() {

	$animations = array(
		'' 					 => esc_html__('Default','resume'),	
		"bigEntrance"        =>  esc_attr__("bigEntrance",'resume'),
        "bounce"             =>  esc_attr__("bounce",'resume'),
        "bounceIn"           =>  esc_attr__("bounceIn",'resume'),
        "bounceInDown"       =>  esc_attr__("bounceInDown",'resume'),
        "bounceInLeft"       =>  esc_attr__("bounceInLeft",'resume'),
        "bounceInRight"      =>  esc_attr__("bounceInRight",'resume'),
        "bounceInUp"         =>  esc_attr__("bounceInUp",'resume'),
        "bounceOut"          =>  esc_attr__("bounceOut",'resume'),
        "bounceOutDown"      =>  esc_attr__("bounceOutDown",'resume'),
        "bounceOutLeft"      =>  esc_attr__("bounceOutLeft",'resume'),
        "bounceOutRight"     =>  esc_attr__("bounceOutRight",'resume'),
        "bounceOutUp"        =>  esc_attr__("bounceOutUp",'resume'),
        "expandOpen"         =>  esc_attr__("expandOpen",'resume'),
        "expandUp"           =>  esc_attr__("expandUp",'resume'),
        "fadeIn"             =>  esc_attr__("fadeIn",'resume'),
        "fadeInDown"         =>  esc_attr__("fadeInDown",'resume'),
        "fadeInDownBig"      =>  esc_attr__("fadeInDownBig",'resume'),
        "fadeInLeft"         =>  esc_attr__("fadeInLeft",'resume'),
        "fadeInLeftBig"      =>  esc_attr__("fadeInLeftBig",'resume'),
        "fadeInRight"        =>  esc_attr__("fadeInRight",'resume'),
        "fadeInRightBig"     =>  esc_attr__("fadeInRightBig",'resume'),
        "fadeInUp"           =>  esc_attr__("fadeInUp",'resume'),
        "fadeInUpBig"        =>  esc_attr__("fadeInUpBig",'resume'),
        "fadeOut"            =>  esc_attr__("fadeOut",'resume'),
        "fadeOutDownBig"     =>  esc_attr__("fadeOutDownBig",'resume'),
        "fadeOutLeft"        =>  esc_attr__("fadeOutLeft",'resume'),
        "fadeOutLeftBig"     =>  esc_attr__("fadeOutLeftBig",'resume'),
        "fadeOutRight"       =>  esc_attr__("fadeOutRight",'resume'),
        "fadeOutUp"          =>  esc_attr__("fadeOutUp",'resume'),
        "fadeOutUpBig"       =>  esc_attr__("fadeOutUpBig",'resume'),
        "flash"              =>  esc_attr__("flash",'resume'),
        "flip"               =>  esc_attr__("flip",'resume'),
        "flipInX"            =>  esc_attr__("flipInX",'resume'),
        "flipInY"            =>  esc_attr__("flipInY",'resume'),
        "flipOutX"           =>  esc_attr__("flipOutX",'resume'),
        "flipOutY"           =>  esc_attr__("flipOutY",'resume'),
        "floating"           =>  esc_attr__("floating",'resume'),
        "hatch"              =>  esc_attr__("hatch",'resume'),
        "hinge"              =>  esc_attr__("hinge",'resume'),
        "lightSpeedIn"       =>  esc_attr__("lightSpeedIn",'resume'),
        "lightSpeedOut"      =>  esc_attr__("lightSpeedOut",'resume'),
        "pullDown"           =>  esc_attr__("pullDown",'resume'),
        "pullUp"             =>  esc_attr__("pullUp",'resume'),
        "pulse"              =>  esc_attr__("pulse",'resume'),
        "rollIn"             =>  esc_attr__("rollIn",'resume'),
        "rollOut"            =>  esc_attr__("rollOut",'resume'),
        "rotateIn"           =>  esc_attr__("rotateIn",'resume'),
        "rotateInDownLeft"   =>  esc_attr__("rotateInDownLeft",'resume'),
        "rotateInDownRight"  =>  esc_attr__("rotateInDownRight",'resume'),
        "rotateInUpLeft"     =>  esc_attr__("rotateInUpLeft",'resume'),
        "rotateInUpRight"    =>  esc_attr__("rotateInUpRight",'resume'),
        "rotateOut"          =>  esc_attr__("rotateOut",'resume'),
        "rotateOutDownRight" =>  esc_attr__("rotateOutDownRight",'resume'),
        "rotateOutUpLeft"    =>  esc_attr__("rotateOutUpLeft",'resume'),
        "rotateOutUpRight"   =>  esc_attr__("rotateOutUpRight",'resume'),
        "shake"              =>  esc_attr__("shake",'resume'),
        "slideDown"          =>  esc_attr__("slideDown",'resume'),
        "slideExpandUp"      =>  esc_attr__("slideExpandUp",'resume'),
        "slideLeft"          =>  esc_attr__("slideLeft",'resume'),
        "slideRight"         =>  esc_attr__("slideRight",'resume'),
        "slideUp"            =>  esc_attr__("slideUp",'resume'),
        "stretchLeft"        =>  esc_attr__("stretchLeft",'resume'),
        "stretchRight"       =>  esc_attr__("stretchRight",'resume'),
        "swing"              =>  esc_attr__("swing",'resume'),
        "tada"               =>  esc_attr__("tada",'resume'),
        "tossing"            =>  esc_attr__("tossing",'resume'),
        "wobble"             =>  esc_attr__("wobble",'resume'),
        "fadeOutDown"        =>  esc_attr__("fadeOutDown",'resume'),
        "fadeOutRightBig"    =>  esc_attr__("fadeOutRightBig",'resume'),
        "rotateOutDownLeft"  =>  esc_attr__("rotateOutDownLeft",'resume')
    );

	return $animations;
}