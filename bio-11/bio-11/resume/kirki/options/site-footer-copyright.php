<?php
$config = resume_kirki_config();

# Footer Copyright
	RESUME_Kirki::add_section( 'dt_footer_copyright', array(
		'title'	=> __( 'Copyright', 'resume' ),
		'description' => __('Footer Copyright Settings','resume'),
		'panel' 		 => 'dt_footer_copyright_panel',
		'priority' => 1
	) );

		# show-copyright-text
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'show-copyright-text',
			'label'    => __( 'Show Copyright Text ?', 'resume' ),
			'section'  => 'dt_footer_copyright',
			'default'  =>  resume_defaults('show-copyright-text'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'resume' ),
				'off' => esc_attr__( 'No', 'resume' )
			)
		) );

		# copyright-text
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'textarea',
			'settings' => 'copyright-text',
			'label'    => __( 'Add Content', 'resume' ),
			'section'  => 'dt_footer_copyright',
			'default'  =>  resume_defaults('copyright-text'),
			'active_callback' => array(
				array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' )
			)
		) );

		# enable-copyright-darkbg
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'enable-copyright-darkbg',
			'label'    => __( 'Enable Copyright Dark BG ?', 'resume' ),
			'section'  => 'dt_footer_copyright',
			'default'  =>  resume_defaults('enable-copyright-darkbg'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'resume' ),
				'off' => esc_attr__( 'No', 'resume' )
			)
		) );		

		# copyright-next
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'select',
			'settings' => 'copyright-next',
			'label'    => __( 'Show Sociable / menu ?', 'resume' ),
			'description'    => __( 'Add description here.', 'resume' ),
			'section'  => 'dt_footer_copyright',
			'default'  => resume_defaults('copyright-next'),
			'choices'  => array(
				'hidden'  => esc_attr__( 'Hide', 'resume' ),
				'disable'  => esc_attr__( 'Disable', 'resume' ),
				'sociable' => esc_attr__( 'Show sociable', 'resume' ),
				'footer-menu' => esc_attr__( 'Show menu', 'resume' ),
			)
		) );

# Footer Social
	RESUME_Kirki::add_section( 'dt_footer_social', array(
		'title'	=> __( 'Social', 'resume' ),
		'description' => __('Footer Social Icons Settings','resume'),
		'panel' 		 => 'dt_footer_copyright_panel',
		'priority' => 2
	) );

		RESUME_Kirki::add_field( $config, array(
			'type'     => 'sortable',
			'settings' => 'footer-sociables',
			'label'    => __( 'Social Icons Order', 'resume' ),
			'section'  => 'dt_footer_social',
			'default'  => resume_defaults('footer-sociables'),
			'choices'  => array(
				"delicious"		=>	esc_attr__( 'Delicious', 'resume' ),
				"deviantart"	=>	esc_attr__( 'Deviantart', 'resume' ),
				"digg"			=>	esc_attr__( 'Digg', 'resume' ),
				"dribbble"		=>	esc_attr__( 'Dribbble', 'resume' ),
				"envelope-open"	=>	esc_attr__( 'Envelope', 'resume' ),
				"facebook"		=>	esc_attr__( 'Facebook', 'resume' ),
				"flickr"		=>	esc_attr__( 'Flickr', 'resume' ),
				"google-plus"	=>	esc_attr__( 'Google Plus', 'resume' ),
				"comment"		=>	esc_attr__( 'GTalk', 'resume' ),
				"instagram"		=>	esc_attr__( 'Instagram', 'resume' ),
				"lastfm"		=>	esc_attr__( 'Lastfm', 'resume' ),
				"linkedin"		=>	esc_attr__( 'Linkedin', 'resume' ),
				"picasa"		=>  esc_attr__( 'Picasa', 'resume' ),
				"myspace"		=>	esc_attr__( 'Myspace', 'resume' ),
				"pinterest"		=>	esc_attr__( 'Pinterest', 'resume' ),
				"reddit"		=>	esc_attr__( 'Reddit', 'resume' ),
				"rss"			=>	esc_attr__( 'RSS', 'resume' ),
				"skype"			=>	esc_attr__( 'Skype', 'resume' ),
				"stumbleupon"	=>	esc_attr__( 'Stumbleupon', 'resume' ),
				"technorati"	=>	esc_attr__( 'Technorati', 'resume' ),
				"tumblr"		=>	esc_attr__( 'Tumblr', 'resume' ),
				"twitter"		=>	esc_attr__( 'Twitter', 'resume' ),
				"viadeo"		=>	esc_attr__( 'Viadeo', 'resume' ),
				"vimeo"			=>	esc_attr__( 'Vimeo', 'resume' ),
				"yahoo"			=>	esc_attr__( 'Yahoo', 'resume' ),
				"youtube"		=>	esc_attr__( 'Youtube', 'resume' ),
			),
			'active_callback' => array(
				array( 'setting' => 'copyright-next', 'operator' => '==', 'value' => 'sociable' ),
			)
		) );

# Footer Copyright Background		
	RESUME_Kirki::add_section( 'dt_footer_copyright_bg', array(
		'title'	=> __( 'Background', 'resume' ),
		'panel' => 'dt_footer_copyright_panel',
		'priority' => 3,
	) );

		# customize-footer-copyright-bg
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'customize-footer-copyright-bg',
			'label'    => __( 'Customize Background ?', 'resume' ),
			'section'  => 'dt_footer_copyright_bg',
			'default'  => resume_defaults('customize-footer-copyright-bg'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'resume' ),
				'off' => esc_attr__( 'No', 'resume' )
			),
			'active_callback' => array(
				array(
					array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
					array( 'setting' => 'copyright-next', 'operator' => 'in', 'value' =>  array( 'sociable', 'footer-menu') )
				)
			)
		));

		# footer-copyright-bg-color
		RESUME_Kirki::add_field( $config, array(
			'type' => 'color',
			'settings' => 'footer-copyright-bg-color',
			'label'    => __( 'Background Color', 'resume' ),
			'section'  => 'dt_footer_copyright_bg',
			'output' => array(
				array( 'element' => '.footer-copyright' , 'property' => 'background-color' )
			),
			'choices' => array( 'alpha' => true ),
			'active_callback' => array(
				array( 'setting' => 'customize-footer-copyright-bg', 'operator' => '==', 'value' => '1' ),
				array(
					array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
					array( 'setting' => 'copyright-next', 'operator' => 'in', 'value' =>  array( 'sociable', 'footer-menu') )				
				)
			)
		));

		# footer-copyright-bg-image
		RESUME_Kirki::add_field( $config, array(
			'type' => 'image',
			'settings' => 'footer-copyright-bg-image',
			'label'    => __( 'Background Image', 'resume' ),
			'description'    => __( 'Add Background Image for footer', 'resume' ),
			'section'  => 'dt_footer_copyright_bg',
			'output' => array(
				array( 'element' => '.footer-copyright' , 'property' => 'background-image' )
			),
			'active_callback' => array(
				array( 'setting' => 'customize-footer-copyright-bg', 'operator' => '==', 'value' => '1' ),
				array(
					array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
					array( 'setting' => 'copyright-next', 'operator' => 'in', 'value' =>  array( 'sociable', 'footer-menu') )		
				)
			)
		));

		# footer-copyright-bg-position
		RESUME_Kirki::add_field( $config, array(
			'type' => 'select',
			'settings' => 'footer-copyright-bg-position',
			'label'    => __( 'Background Image Position', 'resume' ),
			'section'  => 'dt_footer_copyright_bg',
			'output' => array(),
			'default' => 'center',
			'multiple' => 1,
			'choices' => resume_image_positions(),
			'active_callback' => array(
				array( 'setting' => 'customize-footer-copyright-bg', 'operator' => '==', 'value' => '1' ),
				array(
					array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
					array( 'setting' => 'copyright-next', 'operator' => 'in', 'value' =>  array( 'sociable', 'footer-menu') )		
				),
				array( 'setting' => 'footer-copyright-bg-image', 'operator' => '!=', 'value' => '' )				
			)			
		));

		# footer-copyright-bg-repeat
		RESUME_Kirki::add_field( $config, array(
			'type' => 'select',
			'settings' => 'footer-copyright-bg-repeat',
			'label'    => __( 'Background Image Repeat', 'resume' ),
			'section'  => 'dt_footer_copyright_bg',
			'output' => array(),
			'default' => 'repeat',
			'multiple' => 1,
			'choices' => resume_image_repeats(),
			'active_callback' => array(
				array( 'setting' => 'customize-footer-copyright-bg', 'operator' => '==', 'value' => '1' ),
				array(
					array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
					array( 'setting' => 'copyright-next', 'operator' => 'in', 'value' =>  array( 'sociable', 'footer-menu') )		
				),
				array( 'setting' => 'footer-copyright-bg-image', 'operator' => '!=', 'value' => '' )
			)			
		));

# Footer Copyright Typography
	RESUME_Kirki::add_section( 'dt_footer_copyright_typo', array(
		'title'	=> __( 'Typography', 'resume' ),
		'panel' => 'dt_footer_copyright_panel',
		'priority' => 4,
	) );

		# customize-footer-copyright-text-typo
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'customize-footer-copyright-text-typo',
			'label'    => __( 'Customize Copyright Text ?', 'resume' ),
			'section'  => 'dt_footer_copyright_typo',
			'default'  => resume_defaults('customize-footer-copyright-text-typo'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'resume' ),
				'off' => esc_attr__( 'No', 'resume' )
			),
			'active_callback' => array(
				array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' )
			)			
		));

		# footer-copyright-text-typo
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'typography',
			'settings' => 'footer-copyright-text-typo',
			'label'    => __( 'Text Typography', 'resume' ),
			'section'  => 'dt_footer_copyright_typo',
			'output' => array(
				array( 'element' => '.footer-copyright' )
			),
			'default' => resume_defaults( 'footer-copyright-text-typo' ),
			'active_callback' => array(
				array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'customize-footer-copyright-text-typo', 'operator' => '==', 'value' => '1' )
			)		
		));

		# Divider
		RESUME_Kirki::add_field( $config ,array(
			'type'=> 'custom',
			'settings' => 'footer-copyright-text-typo-divider',
			'section'  => 'dt_footer_copyright_typo',
			'default'  => '<div class="customize-control-divider"></div>',
			'active_callback' => array(
				array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'copyright-next', 'operator' => '==', 'value' => 'footer-menu' )
			)			
		));		

		# customize-footer-menu-typo
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'customize-footer-menu-typo',
			'label'    => __( 'Customize Footer Menu ?', 'resume' ),
			'section'  => 'dt_footer_copyright_typo',
			'default'  => resume_defaults('customize-footer-menu-typo'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'resume' ),
				'off' => esc_attr__( 'No', 'resume' )
			),
			'active_callback' => array(
				array( 'setting' => 'copyright-next', 'operator' => '==', 'value' => 'footer-menu' )
			)			
		));

		# footer-menu-typo
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'typography',
			'settings' => 'footer-menu-typo',
			'label'    => __( 'Menu Typography', 'resume' ),
			'section'  => 'dt_footer_copyright_typo',
			'output' => array(
				array( 'element' => '' )
			),
			'default' => resume_defaults( 'footer-menu-typo' ),
			'active_callback' => array(
				array( 'setting' => 'copyright-next', 'operator' => '==', 'value' => 'footer-menu' ),
				array( 'setting' => 'customize-footer-menu-typo', 'operator' => '==', 'value' => '1' )
			)		
		));		