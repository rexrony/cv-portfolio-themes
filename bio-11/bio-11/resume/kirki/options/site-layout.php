<?php
$config = resume_kirki_config();

RESUME_Kirki::add_section( 'dt_site_layout_section', array(
	'title' => __( 'Site Layout', 'resume' ),
	'priority' => 20
) );

	# site-layout
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'site-layout',
		'label'    => __( 'Site Layout', 'resume' ),
		'section'  => 'dt_site_layout_section',
		'default'  => resume_defaults('site-layout'),
		'choices' => array(
			'boxed' =>  RESUME_THEME_URI.'/kirki/assets/images/site-layout/boxed.png',
			'wide' => RESUME_THEME_URI.'/kirki/assets/images/site-layout/wide.png',
		)
	));

	# site-boxed-layout
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'switch',
		'settings' => 'site-boxed-layout',
		'label'    => __( 'Customize Boxed Layout?', 'resume' ),
		'section'  => 'dt_site_layout_section',
		'default'  => '1',
		'choices'  => array(
			'on'  => esc_attr__( 'Yes', 'resume' ),
			'off' => esc_attr__( 'No', 'resume' )
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
		)			
	));

	# body-bg-type
	RESUME_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-type',
		'label'    => __( 'Background Type', 'resume' ),
		'section'  => 'dt_site_layout_section',
		'multiple' => 1,
		'default'  => 'none',
		'choices'  => array(
			'pattern' => esc_attr__( 'Predefined Patterns', 'resume' ),
			'upload' => esc_attr__( 'Set Pattern', 'resume' ),
			'none' => esc_attr__( 'None', 'resume' ),
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-pattern
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'body-bg-pattern',
		'label'    => __( 'Predefined Patterns', 'resume' ),
		'description'    => __( 'Add Background for body', 'resume' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'choices' => array(
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg',
			RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg'=> RESUME_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg',
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'pattern' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)						
	));

	# body-bg-image
	RESUME_Kirki::add_field( $config, array(
		'type' => 'image',
		'settings' => 'body-bg-image',
		'label'    => __( 'Background Image', 'resume' ),
		'description'    => __( 'Add Background Image for body', 'resume' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'upload' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-position
	RESUME_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-position',
		'label'    => __( 'Background Position', 'resume' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-position' )
		),
		'default' => 'center',
		'multiple' => 1,
		'choices' => resume_image_positions(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload') ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-repeat
	RESUME_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-repeat',
		'label'    => __( 'Background Repeat', 'resume' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-repeat' )
		),
		'default' => 'repeat',
		'multiple' => 1,
		'choices' => resume_image_repeats(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload' ) ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));	