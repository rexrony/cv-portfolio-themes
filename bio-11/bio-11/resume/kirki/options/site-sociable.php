<?php
$config = resume_kirki_config();

RESUME_Kirki::add_section( 'dt_sociable_section', array(
	'title' => __( 'Site Sociable', 'resume' ),
	'priority' => 190
) );

	# Delicious
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-delicious',
		'label'	   => __( 'Delicious', 'resume' ),
		'section'  => 'dt_sociable_section',
		'default'  => '#'	
	));

	# Deviantart 
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-deviantart',
		'label'	   => __( 'Deviantart', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Digg 
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-digg',
		'label'	   => __( 'Digg', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Dribbble
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-dribbble',
		'label'	   => __( 'Dribbble', 'resume' ),
		'section'  => 'dt_sociable_section',
		'default'  => '#'
	));

	# Envelope
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-envelope',
		'label'	   => __( 'Envelope', 'resume' ),
		'section'  => 'dt_sociable_section',
	));			

	# Facebook
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-facebook',
		'label'	   => __( 'Facebook', 'resume' ),
		'section'  => 'dt_sociable_section',
		'default'  => '#'
	));

	# Flickr
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-flickr',
		'label'    => __( 'Flickr', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Google Plus
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-google-plus',
		'label'	   => __( 'Google Plus', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# GTalk
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-gtalk',
		'label'	   => __( 'GTalk', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Instagram
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-instagram',
		'label'	   => __( 'Instagram', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Lastfm
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-lastfm',
		'label'	   => __( 'Lastfm', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Linkedin
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-linkedin',
		'label'    => __( 'Linkedin', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Myspace
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-myspace',
		'label'	   => __( 'Myspace', 'resume' ),
		'section'  => 'dt_sociable_section',
	));							

	# Picasa
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-picasa',
		'label'    => __( 'Picasa', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Pinterest
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-pinterest',
		'label'	   => __( 'Pinterest', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Reddit
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-reddit',
		'label'	   => __( 'Reddit', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# RSS
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-rss',
		'label'	   => __( 'RSS', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Skype
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-skype',
		'label'	   => __( 'Skype', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Stumbleupon 
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-stumbleupon',
		'label'	   => __( 'Stumbleupon', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Technorati
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-technorati',
		'label'    => __( 'Technorati', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Tumblr
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-tumblr',
		'label'	   => __( 'Tumblr', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Twitter 
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-twitter',
		'label'	   => __( 'Twitter', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Viadeo
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-viadeo',
		'label'	   => __( 'Viadeo', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Vimeo
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-vimeo',
		'label'	   => __( 'Vimeo', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Yahoo
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-yahoo',
		'label'	   => __( 'Yahoo', 'resume' ),
		'section'  => 'dt_sociable_section',
	));

	# Youtube
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'text',
		'settings' => 'social-youtube',
		'label'	   => __( 'Youtube', 'resume' ),
		'section'  => 'dt_sociable_section',
	));