<?php
$config = resume_kirki_config();

# Breadcrumb Settings
RESUME_Kirki::add_section( 'dt_site_breadcrumb_section', array(
	'title' => __( 'Breadcrumb', 'resume' ),
	'panel' => 'dt_site_breadcrumb_panel',
	'priority' => 1,	
) );

	# show-breadcrumb
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'switch',
		'settings' => 'show-breadcrumb',
		'label'    => __( 'Show Breadcrumb', 'resume' ),
		'section'  => 'dt_site_breadcrumb_section',
		'default'  => '1',
		'choices'  => array(
			'on'  => esc_attr__( 'Yes', 'resume' ),
			'off' => esc_attr__( 'No', 'resume' )
		)
	));

	# breadcrumb-delimiter	
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'select',
		'settings' => 'breadcrumb-delimiter',
		'label'    => __( 'Breadcrumb Delimiter', 'resume' ),
		'section'  => 'dt_site_breadcrumb_section',
		'default'  => resume_defaults( 'breadcrumb-delimiter' ),
		'choices'  => array(
			"fa default" => esc_attr__('Default','resume'),
			"fa fa-angle-double-right" => esc_attr__('Double Angle Right','resume'),
			"fa fa-sort" => esc_attr__('Sort','resume'),
			"fa fa-arrow-circle-right" => esc_attr__('Arrow Circle Right','resume'),
			"fa fa-angle-right" => esc_attr__('Angle Right','resume'),
			"fa fa-caret-right" => esc_attr__('Caret Right','resume'),
			"fa fa-arrow-right" => esc_attr__('Arrow Right','resume'),
			"fa fa-chevron-right" => esc_attr__('Chevron Right','resume'),
			"fa fa-hand-o-right" => esc_attr__('Hand Right','resume'),
			"fa fa-plus" => esc_attr__('Plus','resume'),
			"fa fa-remove" => esc_attr__('Remove','resume'),
			"fa fa-glass" => esc_attr__('Glass','resume'),				
		),
		'active_callback' => array(
			array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' )
		)			
	));

	# breadcrumb-style	
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'select',
		'settings' => 'breadcrumb-style',
		'label'    => __( 'Breadcrumb Style', 'resume' ),
		'section'  => 'dt_site_breadcrumb_section',
		'default'  => resume_defaults( 'breadcrumb-style' ),
		'choices'  => array(
			"default"	=> esc_attr__('Default','resume'),
			"aligncenter"	=> esc_attr__('Align Center','resume'),
			"alignright"	=> esc_attr__('Align Right','resume'),
			"breadcrumb-left"	=> esc_attr__('Left Side Breadcrumb','resume'),
			"breadcrumb-right"	=> esc_attr__('Right Side Breadcrumb','resume'),
			"breadcrumb-top-right-title-center"	=> esc_attr__('Top Right Title Center','resume'),
			"breadcrumb-top-left-title-center"	=> esc_attr__('Top Left Title Center','resume'),				
		),
		'active_callback' => array(
			array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' )
		)			
	));

# Breadcrumb Background Settings
RESUME_Kirki::add_section( 'dt_site_breadcrumb_bg_section', array(
	'title' => __( 'Background', 'resume' ),
	'panel' => 'dt_site_breadcrumb_panel',
	'priority' => 2,	
) );
		# customize-breadcrumb-bg
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'customize-breadcrumb-bg',
			'label'    => __( 'Customize Background ?', 'resume' ),
			'section'  => 'dt_site_breadcrumb_bg_section',
			'default'  => resume_defaults('customize-breadcrumb-bg'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'resume' ),
				'off' => esc_attr__( 'No', 'resume' )
			),
			'active_callback' => array(
				array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' )
			)			
		));

		# breadcrumb-bg-color
		RESUME_Kirki::add_field( $config, array(
			'type' => 'color',
			'settings' => 'breadcrumb-bg-color',
			'label'    => __( 'Background Color', 'resume' ),
			'section'  => 'dt_site_breadcrumb_bg_section',
			'output' => array(
				array( 'element' => '.main-title-section-wrapper:before' , 'property' => 'background-color' )
			),
			'choices' => array( 'alpha' => true ),
			'active_callback' => array(
				array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'customize-breadcrumb-bg', 'operator' => '==', 'value' => '1' )
			)
		));

		# breadcrumb-bg-image
		RESUME_Kirki::add_field( $config, array(
			'type' => 'image',
			'settings' => 'breadcrumb-bg-image',
			'label'    => __( 'Background Image', 'resume' ),
			'description'    => __( 'Add Background Image for breadcrumb', 'resume' ),
			'section'  => 'dt_site_breadcrumb_bg_section',
			'output' => array(
				array( 'element' => '.main-title-section-wrapper:before' , 'property' => 'background-image' )
			),
			'active_callback' => array(
				array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'customize-breadcrumb-bg', 'operator' => '==', 'value' => '1' )
			)
		));

		# breadcrumb-bg-position
		RESUME_Kirki::add_field( $config, array(
			'type' => 'select',
			'settings' => 'breadcrumb-bg-position',
			'label'    => __( 'Background Image Position', 'resume' ),
			'section'  => 'dt_site_breadcrumb_bg_section',
			'output' => array(
				array( 'element' => '.main-title-section-wrapper:before' , 'property' => 'background-position' )				
			),
			'default' => 'center',
			'multiple' => 1,
			'choices' => resume_image_positions(),
			'active_callback' => array(
				array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'customize-breadcrumb-bg', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'breadcrumb-bg-image', 'operator' => '!=', 'value' => '' )
			)
		));

		# breadcrumb-bg-repeat
		RESUME_Kirki::add_field( $config, array(
			'type' => 'select',
			'settings' => 'breadcrumb-bg-repeat',
			'label'    => __( 'Background Image Repeat', 'resume' ),
			'section'  => 'dt_site_breadcrumb_bg_section',
			'output' => array(
				array( 'element' => '.main-title-section-wrapper:before' , 'property' => 'background-repeat' )				
			),
			'default' => 'repeat',
			'multiple' => 1,
			'choices' => resume_image_repeats(),
			'active_callback' => array(
				array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'customize-breadcrumb-bg', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'breadcrumb-bg-image', 'operator' => '!=', 'value' => '' )
			)
		));

# Breadcrumb Typography
	RESUME_Kirki::add_section( 'dt_site_breadcrumb_typo', array(
		'title'	=> __( 'Typography', 'resume' ),
		'panel' => 'dt_site_breadcrumb_panel',
		'priority' => 3,
	) );

		# customize-breadcrumb-title-typo
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'customize-breadcrumb-title-typo',
			'label'    => __( 'Customize Title ?', 'resume' ),
			'section'  => 'dt_site_breadcrumb_typo',
			'default'  => resume_defaults('customize-breadcrumb-title-typo'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'resume' ),
				'off' => esc_attr__( 'No', 'resume' )
			),
			'active_callback' => array(
				array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' )
			)			
		));

		# breadcrumb-title-typo
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'typography',
			'settings' => 'breadcrumb-title-typo',
			'label'    => __( 'Title Typography', 'resume' ),
			'section'  => 'dt_site_breadcrumb_typo',
			'output' => array(
				array( 'element' => '.main-title-section h1, h1.simple-title' )
			),
			'default' => resume_defaults( 'breadcrumb-title-typo' ),
			'active_callback' => array(
				array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'customize-breadcrumb-title-typo', 'operator' => '==', 'value' => '1' )
			)		
		));		

		# customize-breadcrumb-typo
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'customize-breadcrumb-typo',
			'label'    => __( 'Customize Link ?', 'resume' ),
			'section'  => 'dt_site_breadcrumb_typo',
			'default'  => resume_defaults('customize-breadcrumb-typo'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'resume' ),
				'off' => esc_attr__( 'No', 'resume' )
			),
			'active_callback' => array(
				array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' )
			)			
		));

		# breadcrumb-typo
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'typography',
			'settings' => 'breadcrumb-typo',
			'label'    => __( 'Link Typography', 'resume' ),
			'section'  => 'dt_site_breadcrumb_typo',
			'output' => array(
				array( 'element' => 'div.breadcrumb a' )
			),
			'default' => resume_defaults( 'breadcrumb-typo' ),
			'active_callback' => array(
				array( 'setting' => 'show-breadcrumb', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'customize-breadcrumb-typo', 'operator' => '==', 'value' => '1' )
			)		
		));										