<?php
$config = resume_kirki_config();

RESUME_Kirki::add_section( 'dt_custom_js_section', array(
	'title' => __( 'Additional JS', 'resume' ),
	'priority' => 210
) );

	# custom-js
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'switch',
		'settings' => 'enable-custom-js',
		'section'  => 'dt_custom_js_section',
		'label'    => __( 'Enable Custom JS?', 'resume' ),
		'default'  => resume_defaults('enable-custom-js'),
		'choices'  => array(
			'on'  => esc_attr__( 'Yes', 'resume' ),
			'off' => esc_attr__( 'No', 'resume' )
		)		
	));

	# custom-js
	RESUME_Kirki::add_field( $config, array(
		'type'     => 'code',
		'settings' => 'custom-js',
		'section'  => 'dt_custom_js_section',
		'transport' => 'postMessage',
		'label'    => __( 'Add Custom JS', 'resume' ),
		'choices'     => array(
			'language' => 'javascript',
			'theme'    => 'dark',
		),
		'active_callback' => array(
			array( 'setting' => 'enable-custom-js' , 'operator' => '==', 'value' =>'1')
		)
	));