<?php

require_once get_template_directory() . '/kirki/kirki-utils.php';
require_once get_template_directory() . '/kirki/include-kirki.php';
require_once get_template_directory() . '/kirki/kirki.php';

$config = resume_kirki_config();

add_action('customize_register', 'resume_customize_register');
function resume_customize_register( $wp_customize ) {

	$wp_customize->remove_section( 'colors' );
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'static_front_page' );

	$wp_customize->remove_section('themes');
	$wp_customize->get_section('title_tagline')->priority = 10;
}

add_action( 'customize_controls_print_styles', 'resume_enqueue_customizer_stylesheet' );
function resume_enqueue_customizer_stylesheet() {
	wp_register_style( 'resume-customizer-css', RESUME_THEME_URI.'/kirki/assets/css/customizer.css', NULL, NULL, 'all' );
	wp_enqueue_style( 'resume-customizer-css' );	
}

add_action( 'customize_controls_print_footer_scripts', 'resume_enqueue_customizer_script' );
function resume_enqueue_customizer_script() {
	wp_register_script( 'resume-customizer-js', RESUME_THEME_URI.'/kirki/assets/js/customizer.js', array('jquery', 'customize-controls' ), false, true );
	wp_enqueue_script( 'resume-customizer-js' );
}

# Theme Customizer Begins
RESUME_Kirki::add_config( $config , array(
	'capability'    => 'edit_theme_options',
	'option_type'   => 'theme_mod',
) );

	# Site Identity	
		# use-custom-logo
		RESUME_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'use-custom-logo',
			'label'    => __( 'Logo ?', 'resume' ),
			'section'  => 'title_tagline',
			'priority' => 1,
			'default'  => resume_defaults('use-custom-logo'),
			'description' => __('This is test description','resume'),
			'choices'  => array(
				'on'  => esc_attr__( 'Logo', 'resume' ),
				'off' => esc_attr__( 'Site Title', 'resume' )
			)			
		) );

		# custom-logo
		RESUME_Kirki::add_field( $config, array(
			'type' => 'image',
			'settings' => 'custom-logo',
			'label'    => __( 'Logo', 'resume' ),
			'section'  => 'title_tagline',
			'priority' => 2,
			'default' => resume_defaults( 'custom-logo' ),
			'active_callback' => array(
				array( 'setting' => 'use-custom-logo', 'operator' => '==', 'value' => '1' )
			)
		));

		# custom-dark-logo
		RESUME_Kirki::add_field( $config, array(
			'type' => 'image',
			'settings' => 'custom-dark-logo',
			'label'    => __( 'Dark Logo', 'resume' ),
			'section'  => 'title_tagline',
			'priority' => 3,
			'default' => resume_defaults( 'custom-dark-logo' ),
			'active_callback' => array(
				array( 'setting' => 'use-custom-logo', 'operator' => '==', 'value' => '1' )
			)
		));		

		# site-loader
		RESUME_Kirki::add_field( $config, array(
			'type' => 'switch',
			'settings' => 'use-site-loader',
			'label'    => __( 'Use Site Loader?', 'resume' ),
			'section'  => 'title_tagline',
			'priority' => 100,
			'default' => resume_defaults( 'use-site-loader' )
		));

	# Site Layout
	require_once get_template_directory() . '/kirki/options/site-layout.php';

	# Site Skin
	require_once get_template_directory() . '/kirki/options/site-skin.php';

	# Site Breadcrumb
	RESUME_Kirki::add_panel( 'dt_site_breadcrumb_panel', array(
		'title' => __( 'Site Breadcrumb', 'resume' ),
		'description' => __('Site Breadcrumb','resume'),
		'priority' => 25
	) );
	require_once get_template_directory() . '/kirki/options/site-breadcrumb.php';
	
	# Site Header
	RESUME_Kirki::add_panel( 'dt_site_header_panel', array(
		'title' => __( 'Site Header', 'resume' ),
		'description' => __('Site Header','resume'),
		'priority' => 30
	) );
	require_once get_template_directory() . '/kirki/options/site-header.php';

	# Site Menu
	RESUME_Kirki::add_panel( 'dt_site_menu_panel', array(
		'title' => __( 'Site Menu', 'resume' ),
		'description' => __('Site Menu','resume'),
		'priority' => 35
	) );
	require_once get_template_directory() . '/kirki/options/site-menu/navigation.php';		

	# Site Footer I
		RESUME_Kirki::add_panel( 'dt_site_footer_i_panel', array(
			'title' => __( 'Site Footer I', 'resume' ),
			'priority' => 40
		) );
		require_once get_template_directory() . '/kirki/options/site-footer-i.php';

	# Site Footer II
	RESUME_Kirki::add_panel( 'dt_site_footer_ii_panel', array(
		'title' => __( 'Site Footer II', 'resume' ),
		'priority' => 45
	) );
	#require_once get_template_directory() . '/kirki/options/site-footer-ii.php';

	# Site Footer Copyright
	RESUME_Kirki::add_panel( 'dt_footer_copyright_panel', array(
		'title' => __( 'Site Copyright', 'resume' ),
		'priority' => 50
	) );
	require_once get_template_directory() . '/kirki/options/site-footer-copyright.php';

	# Site Sociable
	require_once get_template_directory() . '/kirki/options/site-sociable.php';

	# Additional JS
	require_once get_template_directory() . '/kirki/options/custom-js.php';

	# Typography
	RESUME_Kirki::add_panel( 'dt_site_typography_panel', array(
		'title' => __( 'Typography', 'resume' ),
		'description' => __('Typography Settings','resume'),
		'priority' => 220
	) );	
	require_once get_template_directory() . '/kirki/options/site-typography.php';	
# Theme Customizer Ends